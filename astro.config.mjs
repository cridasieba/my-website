import { defineConfig } from 'astro/config';

import preact from "@astrojs/preact";

// https://astro.build/config
export default defineConfig({
  site: "https://example.com",
  publicDir: './static',
  outDir: './public',
  integrations: [preact()]
});